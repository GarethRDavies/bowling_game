﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMasterOld {

    public enum Action {Tidy, Reset, EndTurn, EndGame};

    private int[] bowls = new int[21];
    private int bowl = 1;

    public static Action NextAction (List<int> pinFalls)
    {
        ActionMasterOld am = new ActionMasterOld();
        Action currentAction = new Action();

        foreach (int pinFall in pinFalls)
        {
            currentAction = am.Bowl(pinFall);
        }

        return currentAction;
    }

	private Action Bowl (int pins)                                                                                           //TODO make bowl method private
    {
        if (pins < 0 || pins > 10)
        {
            throw new UnityException("Invalid number of pins");
        }

        bowls[bowl - 1] = pins;

        if (bowl == 21)
        {
            return Action.EndGame;
        }

        //handle last frame special cases
        if (bowl == 19 && pins == 10)
        {
            bowl += 1;
            return Action.Reset;
        }
        else if (bowl == 20)
        {
            bowl += 1;
            if (AllPinsKnockedDown() && pins != 0)
            {
                return Action.Reset;
            }
            else if (Bowl21Awarded())
            {
                return Action.Tidy;
            }
            else
            {
                return Action.EndGame;
            }
        }


        //If first bowl of frame
        //return tidy
        if (bowl % 2 != 0)                                                                          //First bowl of frames 1-9
        {
            if (pins == 10)
            {
                bowl += 2;
                return Action.EndTurn;
            }
            else
            {
                bowl += 1;
                return Action.Tidy;
            }
        }
        else if (bowl % 2 == 0)                                                                     //Second bowl of frames 1-9
        {
            bowl += 1;
            return Action.EndTurn;
            //end of frame
        }

        throw new UnityException("Not sue what action to return");
    }

    private bool Bowl21Awarded()
    {
        //Remember that arrays start counting at 0
        return (bowls[19 - 1] + bowls[20 - 1] >= 10);
    }

    private bool AllPinsKnockedDown()
    {
        return ((bowls[19 - 1] + bowls[20 - 1]) % 10 == 0);
    }
}
